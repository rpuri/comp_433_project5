package com.book.place.order;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import com.book.domain.Book;
import com.book.domain.BookDAO;

public class OrderDAO {
	private double price1 = 0;
	private int quantity = 0;
	private double totalprice1 = 0;
	private static int orderNumMax;
	private static HashMap<Integer,Order> orders = new HashMap<Integer,Order>();
	
	// In the OrderDAO constructor, I will add few values for Orders.
	public OrderDAO(){
		Order order1 = new Order();
		Address address1 = new Address();
		Address address2 = new Address();
		Book book1 = new Book();
		Book book2 = new Book();
		BookDAO bookDAO = new BookDAO();
		book1 = bookDAO.getBook(1111);
		book2 = bookDAO.getBook(1112);
		order1.addBook(book1);
		order1.addBook(book2);
		address1.setAddress1("802 N Michigan");
		address1.setAddress2("Pearson");
		address1.setCity("Chicago");
		address1.setState("IL");
		address1.setZip(60608);
		address2.setAddress1("ABCD Rd");
		address2.setAddress2("Michigan");
		address2.setCity("Chicago");
		address2.setState("IL");
		address2.setZip(60610);
		order1.setShipAddress(address1);
		order1.setBillAddress(address2);
		order1.setTotalprice(300);
		orderNumMax = 1;
		int orderNum1 = orderNumMax;
		order1.setOrderStatus(1);
		order1.setOrderNum(orderNum1);
		orders.put(orderNum1, order1);
		
		Order order2 = new Order();
		Address address21 = new Address();
		Address address22 = new Address();
		Book book21 = new Book();
		Book book22 = new Book();
		BookDAO bookDAO1 = new BookDAO();
		book21 = bookDAO1.getBook(1113);
		book21 = bookDAO1.getBook(1114);
		order2.addBook(book21);
		order2.addBook(book22);
		address21.setAddress1("802 N Michigan");
		address21.setAddress2("Pearson");
		address21.setCity("Chicago");
		address21.setState("IL");
		address21.setZip(60608);
		address22.setAddress1("ABCD Rd");
		address22.setAddress2("Michigan");
		address22.setCity("Chicago");
		address22.setState("IL");
		address22.setZip(60610);
		order2.setShipAddress(address21);
		order2.setBillAddress(address22);
		order2.setOrderNum(2);
		order2.setTotalprice(400);
		order2.setOrderStatus(2);
		orderNumMax = orderNumMax +1;
		int orderNum2 = orderNumMax;
		orders.put(orderNum2, order2);
		
	}
	public Order setOrder(Order sentorder){
		
		orderNumMax = orderNumMax +1;
		int orderNumSet = orderNumMax;
		sentorder.setOrderNum(orderNumSet);
		ArrayList<Book> inbooks = new ArrayList<Book>();
		BookDAO bookDAO1 = new BookDAO();
		inbooks = sentorder.getBooks();
		Iterator<Book> it = inbooks.iterator();
		while (it.hasNext())
		{ 
			Book iteratebook = it.next();
			System.out.println("iteratebook" + iteratebook);
			if (null != bookDAO1.getBook(iteratebook.getISBN()))
			{	
				price1  = bookDAO1.getBook(iteratebook.getISBN()).getPrice();
				quantity = iteratebook.getCopies();
				totalprice1 = totalprice1 + price1*quantity;
			}
			else
			{	
				sentorder.setMessage("Book not found");
				sentorder.setOrderStatus(5);
				orders.put(orderNumSet, sentorder);
				return sentorder;
			}
		}
		if (totalprice1 != sentorder.getTotalPrice() ){
			sentorder.setMessage("Total price doesn't match with our price");
			sentorder.setOrderStatus(5);
			orders.put(orderNumSet, sentorder);
			return sentorder;
		}
		sentorder.setMessage("Confirmed");
		sentorder.setOrderStatus(1);
		orders.put(orderNumSet, sentorder);
		return sentorder;
	}
	
	public String getOrderStatus(int orderNumber){
		if (null!= orders.get(orderNumber)){
			Order storedOrder =orders.get(orderNumber);
			switch (storedOrder.getOrderStatus()){
				case 1: return "Confirmed";
				case 2: return "in-progress";
				case 3: return "shipped";
				case 4: return "cancelled";
				default: return "error" + storedOrder.getOrderStatus() ;
			}
		}
		else{
			return "Order not found";			
		}
	}
	
	public String cancelOrder(int orderNumber){
		Order storedOrder =orders.get(orderNumber);
		switch (storedOrder.getOrderStatus()){
		case 1: 
			storedOrder.setOrderStatus(4);
			orders.put(orderNumber, storedOrder);
			return "Cancellation Confirmed";
		case 2: 
			storedOrder.setOrderStatus(4);
			orders.put(orderNumber, storedOrder);
			return "Cancellation Confirmed";
		case 3: 
			return "Order Shipped, cannot be canelled";
		case 4: 
			return "Order already cancelled";
		default: 
			return "error while placing order" + storedOrder.getOrderStatus() ;
		
		}
	}
}
