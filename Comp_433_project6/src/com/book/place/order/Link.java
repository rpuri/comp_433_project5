package com.book.place.order;

import java.io.Serializable;

public class Link implements Serializable {
	private static final long serialVersionUID = 3L;
private String URI;
private String rel;
private String mediaType;
public String getURI() {
	return URI;
}
public void setURI(String uRI) {
	URI = uRI;
}
public String getRel() {
	return rel;
}
public void setRel(String rel) {
	this.rel = rel;
}
public String getMediaType() {
	return mediaType;
}
public void setMediaType(String mediaType) {
	this.mediaType = mediaType;
}
}
