package com.book.place.order;

import java.io.Serializable;
import java.util.ArrayList;

import com.book.domain.*;

public class Order implements Serializable {
	private static final long serialVersionUID = 3L;
	private int orderNum;
	private static ArrayList<Book> listofBooks = new ArrayList<Book>();
	private Address shipAddress;
	private Address billAddress;
	private double totalPrice;
	private int orderstatus; 
	private String message;
	// orderstatus 1 = confirmed
	// orderstatus 2 = in-progress
	// orderstatus 3 = shipped
	// orderstatus 4 = cancelled
	// orderstatus 5 = error
	public Order(){};
	
	public void setOrderNum(int orderNum){
		this.orderNum = orderNum;
	}
	public void setListBook(ArrayList<Book> bookList){
		
		listofBooks = new ArrayList<Book>(bookList);
	}
	public void addBook(Book book1){
		listofBooks.add(book1);
	}
	public void setShipAddress(Address shipAddr){
		this.shipAddress = shipAddr;
	}
	public void setBillAddress(Address billAddr){
		this.billAddress = billAddr;
	}
	public void setTotalprice(double totPrice){
		this.totalPrice = totPrice;
	}
	public void setOrderStatus(int ordStatus){
		this.orderstatus = ordStatus;
	}
	public void setMessage(String Message){
		this.message = Message;
	}
	public int getOrderNum(){
		return orderNum;
	}
	public ArrayList<Book> getBooks(){
		return listofBooks;
	}
	public Address getShipAddress(){
		return shipAddress ;
	}
	public Address getBillAddress(){
		return billAddress ;
	}
	public double getTotalPrice(){
		return totalPrice;
	}
	public int getOrderStatus(){
		return orderstatus ;
	}
	public String getMessage(){
		return message;
	}
}
