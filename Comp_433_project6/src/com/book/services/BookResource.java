package com.book.services;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.book.place.order.Order;
import com.book.place.order.OrderDAO;
import com.book.service.representation.BookRepresentation;
import com.book.service.representation.OrderRepresentation;
import com.book.service.representation.OrderStatusRepresentation;
import com.book.service.workflow.BookActivity;
import com.book.service.workflow.OrderActivity;


@Path("/bookservice/")
public class BookResource implements BookService {

	@GET
	@Produces({"application/xml"})
	@Path("/book/{bookId}")
	public BookRepresentation getbook(@PathParam("bookId") int isbn) {
		BookActivity bookactivity = new BookActivity();
		return bookactivity.getBook(isbn);
	}
	
	@POST
	@Produces({"application/xml"})
	@Path("/order")
	public OrderRepresentation setOrder(OrderRepresentation sentorder){
		OrderActivity ordAct = new OrderActivity();
		return ordAct.setOrder(sentorder);
	
	}
	
	@GET
	@Produces({"application/xml"})
	@Path("/order/{orderId}")
	public OrderStatusRepresentation getOrderStatus(@PathParam("orderId") int orderId){
		OrderActivity ordAct = new OrderActivity();
		return ordAct.getOrderStatus(orderId);
	}
	
	@PUT
	@Produces({"application/xml"})
	@Path("/order/cancel")
	public String cancelOrder(int orderId){
		OrderActivity ordAct = new OrderActivity();
		return ordAct.cancelOrder(orderId);
	}


}
