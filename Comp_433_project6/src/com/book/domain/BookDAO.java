package com.book.domain;

import java.util.HashMap;

public class BookDAO {
	
	private Book book5 = new Book(); // this is used to send back nulls
	private static HashMap<Integer,Book> books = new HashMap<Integer,Book>();
	
//not using any database, thus constructor is filling up the data 	
	public BookDAO() {
		Book book1 = new Book();
		book1.setISBN(1111);
		book1.setcopies(1);
		book1.setBookName("Book1111");
		book1.setPrice(10.00);
		books.put(1111, book1);
		Book book2 = new Book();
		book2.setISBN(1112);
		book2.setcopies(2);
		book2.setBookName("Book1112");
		book2.setPrice(20.02);
		books.put(1112, book2);
		Book book3 = new Book();
		book3.setISBN(1113);
		book3.setcopies(3);
		book3.setBookName("Book1113");
		book3.setPrice(30.03);
		books.put(1113, book3);
		Book book4 = new Book();
		book4.setISBN(1114);
		book4.setcopies(4);
		book4.setBookName("Book1114");
		book4.setPrice(40.04);
		books.put(1114, book4);
		
	}
	
	public Book getBook(int isbn) {
		if (null != books.get(isbn))
		return books.get(isbn);
		else
		{
		 //book 5 will send back null values in case book is not in our system
		 return book5;
	
		}
	}
	
}
