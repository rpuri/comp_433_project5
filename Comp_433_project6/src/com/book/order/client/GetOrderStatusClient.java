package com.book.order.client;

import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;

public class GetOrderStatusClient {
	 public static void main(String args[]) throws Exception {
	    	
	    	
	         /*****************************************************************************************
	          * GET METHOD invoke
	          *****************************************************************************************/
	         System.out.println("GET METHOD .........................................................");
	         WebClient getClient = WebClient.create("http://localhost:8080/Comp_433_project6");
	         
	         //Configuring the CXF logging interceptor for the outgoing message
	         WebClient.getConfig(getClient).getOutInterceptors().add(new LoggingOutInterceptor());
	       //Configuring the CXF logging interceptor for the incoming response
	         WebClient.getConfig(getClient).getInInterceptors().add(new LoggingInInterceptor());
	         
	         // change application/xml  to get in xml format
	         getClient = getClient.accept("application/xml").type("application/xml").path("/bookservice/order/5");
	         //getClient = getClient.accept("application/json").type("application/json").path("/employeeservice/employee/XY1111");
	         
	         //The following lines are to show how to log messages without the CXF interceptors
	         String getRequestURI = getClient.getCurrentURI().toString();
	         System.out.println("Client GET METHOD Request URI:  " + getRequestURI);
	         String getRequestHeaders = getClient.getHeaders().toString();
	         System.out.println("Client GET METHOD Request Headers:  " + getRequestHeaders);
	         
	         //to see as raw XML/json
	       
	         String response = getClient.get(String.class);
	         System.out.println("GET METHOD Response: ...." + response);
	         System.exit(0);

	    			
	 }
	 
}
