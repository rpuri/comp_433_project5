package com.book.order.client;

import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;

public class CancelOrderClient {
	 public static void main(String args[]) throws Exception {
		  	
         /*****************************************************************************************
          * PUT METHOD invoke
          *****************************************************************************************/
         System.out.println("PUT METHOD .........................................................");
         WebClient putClient = WebClient.create("http://localhost:8080/Comp_433_project6");
         
         //Configuring the CXF logging interceptor for the outgoing message
         WebClient.getConfig(putClient).getOutInterceptors().add(new LoggingOutInterceptor());
       //Configuring the CXF logging interceptor for the incoming response
         WebClient.getConfig(putClient).getInInterceptors().add(new LoggingInInterceptor());
         
         // change application/xml  to get in xml format
         putClient = putClient.accept("application/xml").type("application/xml").path("/bookservice/order/cancel");
         //getClient = getClient.accept("application/json").type("application/json").path("/employeeservice/employee/XY1111");
         
         //The following lines are to show how to log messages without the CXF interceptors
         String getRequestURI = putClient.getCurrentURI().toString();
         System.out.println("Client GET METHOD Request URI:  " + getRequestURI);
         String getRequestHeaders = putClient.getHeaders().toString();
         System.out.println("Client GET METHOD Request Headers:  " + getRequestHeaders);
         
         //to see as raw XML/json
         String response = putClient.put(2,String.class);
         System.out.println("GET METHOD Response: ...." + response);
         System.exit(0);		
	 }	
}
