package com.book.service.representation;

import java.io.Serializable;
import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.book.place.order.Link;



@XmlRootElement(name = "Book")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class BookRepresentation implements Serializable {
	private static final long serialVersionUID = 1L;
	private int ISBN;
	private int num_copies;
	private double price;
	private String bookName;
	private ArrayList<Link> linkrelList= new ArrayList<Link>();
	

	public BookRepresentation() {}

	public void setISBN (int inpISBN) {
		this.ISBN = inpISBN;
	}

	public void setcopies(int inpCopies) {
		this.num_copies = inpCopies;
	}	

	public void setBookName(String inpBookName) {
		this.bookName = inpBookName;
	}
	public void setPrice(double price){
		this.price= price;
	}
	public Integer getISBN() {
		return ISBN;
	}
	
	public Integer getCopies() {
		return num_copies;
	}
	public String getBookName() {
		return bookName;
	}
	public double getPrice(){
		return price;
	}

	public ArrayList<Link> getLinkrelList() {
		return linkrelList;
	}

	public void setLinkrelList(Link linkobj) {
		linkrelList.add(linkobj);
	}
}