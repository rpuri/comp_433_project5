package com.book.service.representation;

import java.io.Serializable;
import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.book.place.order.Link;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class OrderStatusRepresentation implements Serializable {
	private static final long serialVersionUID = 3L;
	String orderStatus;
	private ArrayList<Link> linkrelList= new ArrayList<Link>();
	
	public OrderStatusRepresentation(){};
	
	public ArrayList<Link> getLinkrelList() {
		return linkrelList;
	}
	public void setLinkrelList(Link objLink) {
		linkrelList.add(objLink);
	}
	public String getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

}
