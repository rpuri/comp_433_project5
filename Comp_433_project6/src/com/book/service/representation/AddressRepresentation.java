package com.book.service.representation;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class AddressRepresentation implements Serializable {
	private static final long serialVersionUID = 2L;
	private String addressLine1;
	private String addressLine2;
	private String city;
	private String state;
	private int zipcode;
	
	public AddressRepresentation(){};

	public void setAddress1(String address1) {
		this.addressLine1 = address1;
	}
	public void setAddress2(String address2) {
		this.addressLine2 = address2;
	}	
	public void setCity(String city) {
		this.city = city;
	}
	public void setState(String state) {
		this.state = state;
	}
	public void setZip(int zip) {
		this.zipcode = zip;
	}

	public String getAddress1(){
		return addressLine1;
	}
	public String getAddress2(){
		return addressLine2;
	}
	public String getCity(){
		return city;
	}
	public String getState(){
		return state;
	}
	public int getZip(){
		return zipcode;
	}

}
