package com.book.service.representation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.book.domain.Book;
import com.book.place.order.Link;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class OrderRepresentation implements Serializable {
	private static final long serialVersionUID = 3L;
	private int orderNum;
	private static ArrayList<Book> listofBooks = new ArrayList<Book>();
	private AddressRepresentation shipAddress;
	private AddressRepresentation billAddress;
	private double totalPrice;
	private int orderstatus; 
	private String message;
	//HATEOS
	
	private ArrayList<Link> linkrelList= new ArrayList<Link>();
	//constructor
	
	// orderstatus 1 = confirmed
	// orderstatus 2 = in-progress
	// orderstatus 3 = shipped
	// orderstatus 4 = cancelled
	// orderstatus 5 = error
	public OrderRepresentation(){};
	
	public void setOrderNum(int orderNum){
		this.orderNum = orderNum;
	}
	public void setListBook(ArrayList<Book> bookList){
		listofBooks = bookList;
	}
	public void addBook(Book book1){
		listofBooks.add(book1);
	}
	public void setShipAddress(AddressRepresentation shipAddr){
		this.shipAddress = shipAddr;
	}
	public void setBillAddress(AddressRepresentation billAddr){
		this.billAddress = billAddr;
	}
	public void setTotalprice(double totPrice){
		this.totalPrice = totPrice;
	}
	public void setOrderStatus(int ordStatus){
		this.orderstatus = ordStatus;
	}
	public void setMessage(String Message){
		this.message = Message;
	}
	public int getOrderNum(){
		return orderNum;
	}
	public ArrayList<Book> getBooks(){
		return listofBooks;
	}
	public AddressRepresentation getShipAddress(){
		return shipAddress ;
	}
	public AddressRepresentation getBillAddress(){
		return billAddress ;
	}
	public double getTotalPrice(){
		return totalPrice;
	}
	public int getOrderStatus(){
		return orderstatus ;
	}
	public String getMessage(){
		return message;
	}
	public void setLinkrelList(Link objLink) {
		linkrelList.add(objLink);
	}

}
