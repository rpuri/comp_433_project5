package com.book.service.workflow;

import com.book.place.order.Address;
import com.book.place.order.Link;
import com.book.place.order.Order;
import com.book.place.order.OrderDAO;
import com.book.service.representation.AddressRepresentation;
import com.book.service.representation.OrderRepresentation;
import com.book.service.representation.OrderStatusRepresentation;

public class OrderActivity  {
	
	public OrderRepresentation setOrder(OrderRepresentation sentorder) {
		OrderDAO dao = new OrderDAO();
		Order order = new Order();
		Address address1 = new Address();
		Address address2 = new Address();
		AddressRepresentation addrep = new AddressRepresentation();
		addrep = sentorder.getShipAddress();
		AddressRepresentation addrep2 = new AddressRepresentation();
		addrep2 = sentorder.getBillAddress();
		Link objLink;
		
		
		address1.setAddress1(addrep.getAddress1());
		address1.setAddress2(addrep.getAddress2());
		address1.setCity(addrep.getCity());
		address1.setState(addrep.getState());
		address1.setZip(addrep.getZip());
		
		address2.setAddress1(addrep2.getAddress1());
		address2.setAddress2(addrep2.getAddress2());
		address2.setCity(addrep2.getCity());
		address2.setState(addrep2.getState());
		address2.setZip(addrep2.getZip());
		
		
		order.setListBook(sentorder.getBooks());
		order.setTotalprice(sentorder.getTotalPrice());
		order.setShipAddress(address1);
		order.setBillAddress(address2);
		
		
		order = dao.setOrder(order);
		
		sentorder.setMessage(order.getMessage());
		sentorder.setOrderNum(order.getOrderNum());
		sentorder.setOrderStatus(order.getOrderStatus());
		//HATEAOS
		objLink = new Link();
		objLink.setRel(" ");
		objLink.setURI(" ");
	
		objLink.setMediaType(" ");
	
		if (order.getOrderStatus() ==1) {
			objLink.setRel("Cancel");
			objLink.setURI("http://localhost:8080/Comp_433_project6/bookservice/order/cancel");	
			objLink.setMediaType("application/vnd.bookstore+xml");
		}
		sentorder.setLinkrelList(objLink);

		
		return sentorder;
	}
	public OrderStatusRepresentation getOrderStatus(int orderNumber){
		OrderDAO dao = new OrderDAO();
		OrderStatusRepresentation orderStatusRep = new OrderStatusRepresentation(); 
		orderStatusRep.setOrderStatus(dao.getOrderStatus(orderNumber));
		Link objLink = new Link();
		if ((dao.getOrderStatus(orderNumber).equals("confirmed"))||(dao.getOrderStatus(orderNumber).equals("in-progress"))){
			objLink.setRel("Cancel");
			objLink.setURI("http://localhost:8080/Comp_433_project6/bookservice/order/cancel");
		
			objLink.setMediaType("application/vnd.bookstore+xml");
		}
		else{
			objLink.setRel(" ");
			objLink.setURI(" ");
			objLink.setMediaType(" ");
		}
		orderStatusRep.setLinkrelList(objLink);
		
		return orderStatusRep;
	}
	public String cancelOrder(int orderNumber){
		OrderDAO dao = new OrderDAO();
		return dao.cancelOrder(orderNumber);
	}
}
