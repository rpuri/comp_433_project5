package com.book.service.workflow;

import com.book.domain.Book;
import com.book.domain.BookDAO;
import com.book.place.order.Link;
import com.book.service.representation.BookRepresentation;

public class BookActivity {

	public BookRepresentation getBook(Integer isbn) {
		BookDAO dao = new BookDAO();
		Book book = dao.getBook(isbn);
		BookRepresentation bookrep = new BookRepresentation();
		bookrep.setBookName(book.getBookName());
		bookrep.setcopies(book.getCopies());
		bookrep.setISBN(book.getISBN());
		bookrep.setPrice(book.getPrice());
		Link nextLink = new Link();
		nextLink.setURI(" ");
		nextLink.setMediaType(" ");
		nextLink.setRel(" ");	
		if (book.getISBN() >0){
			nextLink.setURI("http://localhost:8080/Comp_433_project6/bookservice/order");
			nextLink.setMediaType("application/vnd.bookstore+xml");
			nextLink.setRel("Order");	
		}
			
		bookrep.setLinkrelList(nextLink);
		return bookrep;
	}
}
