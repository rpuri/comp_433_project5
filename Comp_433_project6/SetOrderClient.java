package com.book.order.client;


import java.util.ArrayList;

import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;

import com.book.domain.*;
import com.book.place.order.Address;
import com.book.place.order.Order;
import com.book.service.representation.AddressRepresentation;
import com.book.service.representation.BookRepresentation;
import com.book.service.representation.OrderRepresentation;
import com.book.service.workflow.BookActivity;

public class SetOrderClient {

			
	 public static void main(String args[]) throws Exception {

    	
	    	 /*****************************************************************************************
	         * POST METHOD invoke
	        *****************************************************************************************/
	        System.out.println("POST METHOD .........................................................");
	        WebClient postClient = WebClient.create("http://localhost:8080/Comp_433_project6");
	        WebClient.getConfig(postClient).getOutInterceptors().add(new LoggingOutInterceptor());
	        WebClient.getConfig(postClient).getInInterceptors().add(new LoggingInInterceptor());
	                 
	        // change application/xml  to application/json get in json format
	        postClient = postClient.accept("application/xml").type("application/xml").path("/bookservice/order");
	     	
	        String postRequestURI = postClient.getCurrentURI().toString();
	        System.out.println("Client POST METHOD Request URI:  " + postRequestURI);
	        String postRequestHeaders = postClient.getHeaders().toString();
	        System.out.println("Client POST METHOD Request Headers:  " + postRequestHeaders);
	       
	        
	     	
	    			
	    	
	    	OrderRepresentation order1 = new OrderRepresentation();
			AddressRepresentation address1 = new AddressRepresentation();
			AddressRepresentation address2 = new AddressRepresentation();
			Book book1 = new Book();
			Book book2 = new Book();
			BookDAO bookdao = new BookDAO();
			book1 = bookdao.getBook(1115);
			book2 = bookdao.getBook(1113);
			order1.addBook(book1);
			order1.addBook(book2);
			address1.setAddress1("802 N Michigan");
			address1.setAddress2("Pearson");
			address1.setCity("Chicago");
			address1.setState("IL");
			address1.setZip(60608);
			address2.setAddress1("ABCD Rd");
			address2.setAddress2("Michigan");
			address2.setCity("Chicago");
			address2.setState("IL");
			address2.setZip(60610);
			order1.setShipAddress(address1);
			order1.setBillAddress(address2);
			order1.setTotalprice(300);
			order1.setMessage(" ");
			int orderNumMax = 1;
			int orderNum1 = orderNumMax;
			order1.setOrderNum(orderNum1);
			
			
	    	
	    	String responsePost =  postClient.post(order1, String.class);
	        System.out.println("POST MEDTHOD Response ........." + responsePost);
	    	System.exit(0);

	    }
}
